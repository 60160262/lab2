// const isUserNameValid = function(username) {
//     return true;    
// }

module.exports = {
    isUserNameValid: function (username) {
        if (username.length < 3 || username.length > 15) {
            return false;
        }
        if (username.toLowerCase() !== username) {
            return false;
        }
        return true;
    },
    isAgeValid: function (age) {

        // if(age.match(/^[0-9]/)){
        //     return true;
        // }
        if (typeof age !== 'number') {
            return false;
        }
        if (age < 18 || age > 100) {
            return false;
        }
        return true;
    },
    isPasswordValid: function (password) {
        var num = /^[0-9]/+$;
        var char = /^[A-Z]/+$;
        var form = /^[!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?]*$/;
        var checknum = 0;
        var checkchar = false;
        var checkform = false;
        if (password > 7) {
            for (i = 0; i < password.length; i++) {
                if (password[i].match(num)) {
                    checknum++;
                }
                if (password[i].match(char)) {
                    checkchar = true;
                }
                if (password[i].match(form)) {
                    checkform = true;
                }
            } if (checknum > 2 && checkchar == true && checkform == true) {
                return true;
            } return false;
        } return false;
    },
    isDateValid: function (day, month, year) {
        if (day > 0 && day <= 31 &&
            month > 0 && month <= 12 &&
            year >= 1970 && year <= 2020) {

            if (month == 2) {
                if (year % 400 == 0 && year % 100 == 0) {
                    if (day <=29) {
                        return true;
                    } else {
                        return false;
                        break;
                    }
                }
            }
            if (month == 4 || month == 6 || month == 9 || month == 11) {
                if (day <= 30) {
                    return true;
                } else {
                    return false;
                    break;
                }
            }return true;

        } return false;
    }


}